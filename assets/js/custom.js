// Animation
function setAnimation() {

    let aminate = document.querySelectorAll(".aminate");

    for (let i = 1; i < aminate.length; i++) {
        const windowHeight = window.innerHeight;
        const elementTop = aminate[i].getBoundingClientRect().top;
        const elementVisible = 100;

        if (elementTop < windowHeight - elementVisible) {
            aminate[i].classList.add("active");
        } else {
            // aminate[i].classList.remove("active");
        }
    }
}
function setAnimationHero() {
    let aminate = document.querySelectorAll(".aminate");
    aminate[0].classList.add("active");
}
setAnimationHero();
window.addEventListener("scroll", setAnimation);

// Change Price
const ANNUAL = 29;
const MONTHLY = 49;

const checkbox = document.getElementById('togglePrice');
const buttonToggle = document.getElementById('toggleButton')
const price = document.getElementById('price');


function changePrice(start, end) {
    if (start < end) {
        let time = setInterval(() => {
            start++
            if (start === end)
                clearInterval(time)
            price.textContent = start;
        }, 20)

    }

    if (start > end) {
        let time = setInterval(() => {
            start--;
            if (start === end)
                clearInterval(time)
            price.textContent = start;
        }, 20)

    }
}
toggleButton.addEventListener('click', () => {
    if (!checkbox.checked) {
        changePrice(ANNUAL, MONTHLY)
    }

    if (checkbox.checked) {
        changePrice(MONTHLY, ANNUAL)
    }
});

// Load number stat
const satisfaction = document.getElementById("satisfaction");
const hour = document.getElementById("hour");
const day = document.getElementById("day");
const sale = document.getElementById("sale");

function loadNumber(start, end, view) {
    let time = setInterval(() => {
        start++
        if (start === end)
            clearInterval(time)
        view.textContent = start;
    }, 20)
}

loadNumber(0, 100, satisfaction)
loadNumber(0, 24, hour)
loadNumber(0, 7, day)
loadNumber(0, 100, sale)

// Slider 
const slides = document.querySelectorAll(".landkit__testimonial__slide")
const images = document.querySelectorAll(".landkit__testimonial__slide__image")
const inners = document.querySelectorAll(".landkit__testimonial__slide__inner")

const next = document.getElementById('next')
const prev = document.getElementById('prev')

let currentSlide = 0;
let maxSlide = slides.length - 1;

const goToNext = () => {
    if (currentSlide === maxSlide) {
        currentSlide = 0;
    } else {
        currentSlide++;
    }

    slides.forEach((slide, index) => {
        slide.style.transform = `translateX(${100 * (index - currentSlide)}%)`;
        slide.style.transition = 'ease-out 0.4s'
    });
}
const goToPrev = () => {
    if (currentSlide === 0) {
        currentSlide = maxSlide;
    } else {
        currentSlide--;
    }

    slides.forEach((slide, index) => {
        slide.style.transform = `translateX(${100 * (index - currentSlide)}%)`;
        slide.style.transition = 'ease-out 0.4s'
    });
}

next.addEventListener("click", goToNext);

prev.addEventListener("click", goToPrev);

document.addEventListener('keydown', function (e) {
    if (e.code === 'ArrowRight') {
        goToNext();
    } else if (e.code === 'ArrowLeft') {
        goToPrev();
    }
});

// Toggle Menu
const showHeaderMobile = document.getElementById('showHeaderMobile');
const closeHeaderMobile = document.getElementById('closeHeaderMobile');

const navbar = document.getElementById('navbar');
const menu = document.getElementById('navbarMenu');

const dropdownToggleList = document.querySelectorAll('.dropdown-toggle');
const dropdownMenuList = document.querySelectorAll('.dropdown-menu');

// Toggle navbar menu
showHeaderMobile.addEventListener('click', () => {
    menu.classList.add('show');

    document.body.style.overflowY = 'hidden';
    navbar.style.height = '100vh';
});

closeHeaderMobile.addEventListener('click', () => {
    menu.classList.remove('show');

    document.body.style.overflowY = 'auto';
    navbar.style.height = 'auto';
});

// Toggle dropdown
dropdownToggleList.forEach((item, index) =>
    item.addEventListener('click', () => {
        dropdownMenuList[index].classList.toggle('show');
        item.classList.toggle('active');
    })
);

// Typewritter
function TxtType(el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 2000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
};

TxtType.prototype.tick = function () {
    let i = this.loopNum % this.toRotate.length;
    let fullTxt = this.toRotate[i];

    if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    this.el.innerHTML = '<span class="wrap">' + this.txt + '</span>';

    let that = this;
    let delta = 200 - Math.random() * 100;

    if (this.isDeleting) { delta /= 2; }

    if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
    }

    setTimeout(function () {
        that.tick();
    }, delta);
};

window.onload = function () {
    let elements = document.getElementsByClassName('typewrite');
    for (let i = 0; i < elements.length; i++) {
        let toRotate = elements[i].getAttribute('data-type');
        let period = elements[i].getAttribute('data-period');
        if (toRotate) {
            new TxtType(elements[i], JSON.parse(toRotate), period);
        }
    }
    // INJECT CSS
    let css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
    document.body.appendChild(css);
};

// Validate Form
const form = document.getElementById('form')
const nameError = document.getElementById('nameError')
const emailError = document.getElementById('emailError')
const passwordError = document.getElementById('passwordError')

// validate required fields
function isValidName(name) {
    let valid = true;
    if (name.value.length == 0) {
        nameError.innerHTML = "Please Enter Name";
        valid = false;
    } else {
        if (name.value.length < 8) {
            nameError.innerHTML = "Name must greater than 8"
            valid = false;
        }
        else {
            nameError.innerHTML = '';
            valid = true;
        }
    }
    return valid
}

// validate email as required field and format
function trim(s) {
    return s.replace(/^\s+|\s+$/, '');
}

function isValidEmail(email) {
    let valid = true;
    let temail = trim(email.value); // value of field with whitespace trimmed off
    let emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/;
    let illegalChars = /[\(\)\<\>\,\;\:\\\"\[\]]/;

    if (email.value == "") {

        emailError.innerHTML = "Please enter an email address.";
        valid = false;
    } else {
        if (email.value.length < 8) {
            emailError.innerHTML = "Email must greater than 8";
            valid = false;
        } else if (!emailFilter.test(temail)) { //test email for illegal characters

            emailError.innerHTML = "Please enter a valid email address.";
            valid = false;
        } else if (email.value.match(illegalChars)) {
            valid = false;
            emailError.innerHTML = "Email contains invalid characters.";
        } else {
            email.style.background = 'White';
            emailError.innerHTML = '';
            valid = true;
        }
    }
    return valid;
}




// validate required fields
function isValidPass(password) {
    let valid = true;

    if (password.value.length == 0) {
        passwordError.innerHTML = "Please Enter password";
        valid = false
    } else {
        if (password.value.length < 8) {
            passwordError.innerHTML = 'Password must greater than 8';
            valid = false
        }
        else {
            passwordError.innerHTML = '';
            valid = true
        }
    }
    return valid;
}


function handleSubmitForm(item, event) {
    event.preventDefault();
    if (validate(item)) {
        form.reset();
    }
    else {
        return false
    }
}

function validate(item) {
    const emailValid = isValidEmail(item.email);
    const nameValid = isValidName(item.name);
    const passValid = isValidPass(item.password)

    let pass = nameValid && emailValid && passValid;

    if (pass) {
        console.log("Name: ", item.name.value, "Email: ", item.email.value, "Password: ", item.password.value);
        return pass
    }
    else
        return false
}